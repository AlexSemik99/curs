﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace FS.WorkingPart
{
    class Groups
    {
        List<Struct.Group> groups = new List<Struct.Group>();
        Struct.MFT group;
        ASFS asfs;
        int Size = 0;
        public Groups()
        {
            Size = Marshal.SizeOf(typeof(FS.Struct.Group));
        }
        public UInt32 GetBlockSize(UInt32 SectorBlock)
        {
            return Convert.ToUInt32(Math.Ceiling(Convert.ToDouble(Size * groups.Count) / SectorBlock));
        }
        public int GetSizeBytes()
        {
            return Size * groups.Count;
        }
        public bool AddGroup(string NameGroup, UInt32 AccessLevel)
        {
            Struct.Group gr = new Struct.Group
            {
                Access_Level = AccessLevel
            };
            if (NameGroup.Length > 20) gr.Name_Group = NameGroup.Substring(0, 20).ToCharArray();
            else if (NameGroup.Length == 20) gr.Name_Group = NameGroup.ToCharArray();
            else
            {
                gr.Name_Group = new char[20];
                string str = "";
                for (int i = 0; i < 20 - NameGroup.Length; i++)
                    str += ' ';
                NameGroup.ToCharArray().CopyTo(gr.Name_Group, 0);
                str.ToCharArray().CopyTo(gr.Name_Group, NameGroup.Length);
            }
            FS.Struct.Group g = groups.Find((x) => x.Name_Group == gr.Name_Group);
            if (g.Name_Group != null)
                return false;
            gr.Id_Group = Convert.ToUInt32(groups.Count) + 1;
            groups.Add(gr);
            return true;
        }
        public byte[] Convert_To_byte()
        {
            byte[] bt = new byte[Size * groups.Count];
            byte[] b; int k = 0;

            for (int i = 0; i < groups.Count; i++)
            {
                b = FunctionsForFS.GetBytes(groups[i]);
                b.CopyTo(bt, k);
                k += b.Length;
            }
            return bt;
        }
        public void Initialize(ASFS fs, Struct.MFT igroup)
        {
            asfs = fs;
            group = igroup;
        }
        public Struct.Group LoadAllGroup(UInt32 ID)
        {
            Struct.Group g = new Struct.Group();
            if (Convert.ToUInt32(ID * Size) > group.Busy_Bytes)
                return new Struct.Group();
            FunctionsForFS.FromBytes(asfs.ReadBytes(group.AdressBlockFile, Convert.ToUInt32(ID * Size), Convert.ToUInt32(Size), null), out g);
            return g;
        }
        public UInt32 GetGroupLevel(Users _users, UInt32 ComeUserID)
        {
            return LoadAllGroup((_users.GetUsers().Find((x) => x.Id_User == ComeUserID).Id_Group)).Access_Level;
        }
        public Struct.Group[] GetGroups()
        {
            Struct.Group gr = new Struct.Group();

            Struct.Group[] Temp = new Struct.Group[group.Busy_Bytes / Convert.ToUInt32(Size)];
            for (uint i = 0; i < group.Busy_Bytes / Convert.ToUInt32(Size); i++)
            {
                Struct.Group g = new Struct.Group();
                if (Convert.ToUInt64(i * Size) > group.Busy_Bytes)
                    new Struct.Group();
                FunctionsForFS.FromBytes(asfs.ReadBytes(group.AdressBlockFile, Convert.ToUInt32(i * Size), Convert.ToUInt32(Size), null), out g);
                Temp[i] = g;
            }
            return Temp;
        }
    }
}
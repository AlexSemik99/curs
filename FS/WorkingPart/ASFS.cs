﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace FS.WorkingPart
{
    class ASFS
    {
        public static string Path_To_File = @"ASFS.cn";
        Struct.ASFS[] FAT;
        List<Struct.ASFS> AdressNextBlock = new List<Struct.ASFS>();
        int Size = 0;
        private UInt32 EndOfFile = UInt32.MaxValue - 1,
                       FreeSector = UInt32.MaxValue - 2;
        public UInt32 CountRecInOneBlock = 0,   /*Количество записей в одном блоке*/
               BootBlockSize = 0,               /*Размер блока*/
               StartFSBlock = 0;                /*Начальный блок ASFS*/
        int BootSizeSuperblock = 0;             /*Размер загрузочного блока*/
        ulong FSLengthByBytes = 0;
        #region Установка файловой системы
        public UInt32 CountBlock = 0;
        public ASFS()
        {
            Size = Marshal.SizeOf(typeof(Struct.ASFS));     /*Размер 1-ой записи в файле FS*/
        }
        public bool CheckFile()
        {
            if (File.Exists(Path_To_File)) return true;
            else return false;
        }
        public UInt32 GetBlockSize(UInt32 SectorBlock)
        {
            return Convert.ToUInt32(Math.Ceiling(Convert.ToDouble(Size * FAT.Length) / SectorBlock));
        }                   /*Размер таблицы в блоках*/
        public int GetSizeBytes()
        {
            return Size * FAT.Length;
        }
        public UInt32 SelectionSizeBlock(UInt32 DiskSize)
        {
            UInt32 ClasterSize = 512;
            while (DiskSize / ClasterSize > UInt32.MaxValue - 3)
                ClasterSize *= 2;
            FAT = new Struct.ASFS[Convert.ToUInt32(DiskSize / ClasterSize)];
            CountBlock = Convert.ToUInt32(DiskSize / ClasterSize);
            for (UInt32 i = 0; i < FAT.Length; i++)
                FAT[i].ValueNextBlock = FreeSector;
            return ClasterSize;
        }               /*Для совсем крайних случаев*/
        public void AddRecordInFAT(UInt32 StartBlock, UInt32 Lenght)
        {
            for (UInt32 i = StartBlock; i < StartBlock + Lenght; i++)
            {
                if (i + 1 == StartBlock + Lenght)
                {
                    FAT[i].ValueNextBlock = EndOfFile;
                    break;
                }
                else
                {
                    FAT[i].ValueNextBlock = i + 1;
                }
            }
        }    /*Length - длина файла в блоках*/
        public byte[] Convert_To_byte()
        {
            byte[] bt = new byte[Size * FAT.Length];
            int k = 0;
            byte[] b;
            for (int i = 0; i < FAT.Length; i++)
            {
                b = FunctionsForFS.GetBytes(FAT[i]);
                b.CopyTo(bt, k);
                k += b.Length;
            }
            return bt;
        }
        #endregion
        public void Initialize(UInt32 BlockSize, int SizeSuperblock)
        {
            BootBlockSize = BlockSize;
            BootSizeSuperblock = SizeSuperblock;
            CountRecInOneBlock = BlockSize / Convert.ToUInt32(Size);
        }           /*Для навигации в FAT*/
        public void Initialize2(UInt32 InStartFSBlock, ulong FileLength)
        {
            StartFSBlock = InStartFSBlock;
            AdressNextBlock.Add(new Struct.ASFS() { ValueNextBlock = InStartFSBlock });
            bool ReadInfo = true;
            FileStream stream = new FileStream(Path_To_File, FileMode.Open);
            while (ReadInfo)
            {
                stream.Position = StartFSBlock * BootBlockSize + InStartFSBlock * Size + BootSizeSuperblock;
                byte[] Info = new byte[Size];
                stream.Read(Info, 0, Size);
                FunctionsForFS.FromBytes(Info, out Struct.ASFS fs);
                InStartFSBlock = fs.ValueNextBlock;
                AdressNextBlock.Add(fs);
                if (fs.ValueNextBlock == UInt32.MaxValue - 1)
                    ReadInfo = false;
            }
            stream.Close();
            FSLengthByBytes = FileLength;
        }
        public void ReadInFAT(UInt32 StartBlock)
        {
            UInt32 NumFSRec = StartBlock;
            AdressNextBlock.Add(new Struct.ASFS() { ValueNextBlock = NumFSRec });
            FileStream fs = new FileStream(Path_To_File, FileMode.Open);
            bool FindEndOfFile = false;
            while (!FindEndOfFile)          /*Пока не найду конец файла*/
            {
                fs.Position = StartBlock * BootBlockSize + NumFSRec * Size + BootSizeSuperblock;
                byte[] byte_array = new byte[Size];
                fs.Read(byte_array, 0, Size);
                FunctionsForFS.FromBytes(byte_array, out Struct.ASFS asfs);
                StartBlock = asfs.ValueNextBlock;
                AdressNextBlock.Add(asfs);
                if (asfs.ValueNextBlock == EndOfFile)
                    FindEndOfFile = true;
            }   /*Чтение FAT*/
        }
        public byte[] ReadBytes(UInt32 StartPosition, UInt32 StartNumByte, UInt32 FileLength, FileStream fs)
        {
            byte[] ArrayForRead = new byte[FileLength];
            int CountReadByte = 0, CountRead = 0;                         /*Количество байт, которые нужно считать; Количество считанных байт*/
            UInt32 NumBlockForRead = 0;                                   /*Номер блока по счёту*/
            UInt32 PositionForRead = StartPosition;
            if (fs ==null)
            {
                using(fs = new FileStream(Path_To_File, FileMode.Open))
                {
                    while (CountRead != FileLength)                                  /*while - болеее надёжный, чем for в этой ситуации*/
                    {
                        PositionForRead = SearchNeedBlock(PositionForRead, StartNumByte, out NumBlockForRead, out UInt32 End, fs);
                        if (PositionForRead == EndOfFile)
                            PositionForRead = End;
                        UInt32 Offset = StartNumByte - NumBlockForRead * BootBlockSize;        /*Расчёт смещения*/
                        if (Offset + FileLength - CountRead > BootBlockSize)
                        {
                            CountReadByte = Convert.ToInt32(BootBlockSize - Offset);
                            StartNumByte += Convert.ToUInt32(CountReadByte);
                        }
                        else
                            CountReadByte = Convert.ToInt32(FileLength - CountRead);
                        fs.Position = BootSizeSuperblock + PositionForRead * BootBlockSize + Offset;
                        fs.Read(ArrayForRead, CountRead, CountReadByte);

                        CountRead += CountReadByte;
                    }
                }
            }
            while (CountRead != FileLength)                                  /*while - болеее надёжный, чем for в этой ситуации*/
            {
                PositionForRead = SearchNeedBlock(PositionForRead, StartNumByte, out NumBlockForRead, out UInt32 End, fs);
                if (PositionForRead == EndOfFile)
                    PositionForRead = End;
                UInt32 Offset = StartNumByte - NumBlockForRead * BootBlockSize;        /*Расчёт смещения*/
                if (Offset + FileLength - CountRead > BootBlockSize)
                {
                    CountReadByte = Convert.ToInt32(BootBlockSize - Offset);
                    StartNumByte += Convert.ToUInt32(CountReadByte);
                }
                else
                    CountReadByte = Convert.ToInt32(FileLength - CountRead);
                fs.Position = BootSizeSuperblock + PositionForRead * BootBlockSize + Offset;
                fs.Read(ArrayForRead, CountRead, CountReadByte);
                
                CountRead += CountReadByte;
            }
            //fs.Close();
            return ArrayForRead;
        }   /*Чтение побайтово*/
        public bool CopyFile(UInt32 IndentFor12PersentForMFT, UInt32 StartBlockOriginalFile, ulong OriginalFileLength, out UInt32 StartBlockForCopyFile, out string ErrorMessage)
        {
            StartBlockForCopyFile = 0; ErrorMessage = "";
            UInt32 CountBlockForCopy = Convert.ToUInt32(Math.Ceiling(Convert.ToDouble(OriginalFileLength) / BootBlockSize)), CheckCountCopyBlocks = 0, Block = 0;
            var FreeArea = CountNullBlockForWriteAndCopy(IndentFor12PersentForMFT, null);
            if (CountBlockForCopy > FreeArea)
            {
                ErrorMessage = "Недостаточно места!";
                return false;
            }
            /*----------------------\КОПИРОВАНИЕ ФАЙЛА/----------------------*/
            StartBlockForCopyFile = CountNullBlockForWriteAndCopy(IndentFor12PersentForMFT, null);
            byte[] ArrayForCopyFile;
            UInt32 CopyFileLength = 0;
            do
            {
                ArrayForCopyFile = new byte[BootBlockSize];
                ArrayForCopyFile = ReadBytes(StartBlockOriginalFile, Convert.ToUInt32(BootBlockSize), Convert.ToUInt32(CopyFileLength), null);
                if (!WriteFileByBytes(ArrayForCopyFile, StartBlockForCopyFile, Convert.ToUInt32(CopyFileLength), OriginalFileLength, Block, IndentFor12PersentForMFT, out Block, out ErrorMessage))
                    CheckCountCopyBlocks += Block;
                CopyFileLength += Convert.ToUInt32(BootBlockSize);
            } while (CopyFileLength < OriginalFileLength);
            if (CountBlockForCopy != CheckCountCopyBlocks)
                return false;
            else
                return true;
        }
        public bool DeleteFile(UInt32 StartBlockOriginalFile)
        {
            UInt32 f = 0, SearchBlocksForDelete = 0, Check = EndOfFile, LastBlock = 0;
            FileStream fs = new FileStream(Path_To_File, FileMode.Open);
            while (true)
            {
                SearchBlocksForDelete = SearchNeedBlock(StartBlockOriginalFile, f * BootBlockSize, out SearchBlocksForDelete, out LastBlock, fs);
                if (Check == SearchBlocksForDelete)
                {
                    fs.Close();
                    return false;
                }
                if (SearchBlocksForDelete == EndOfFile)
                    SearchBlocksForDelete = LastBlock;
                ChangeStructureASFS(SearchBlocksForDelete, 0, fs);
                if (SearchBlocksForDelete == EndOfFile)
                {
                    fs.Close();
                    return true;
                }
                Check = LastBlock;
            }
        }
        public bool WriteFileByBytes(byte[] ArrayForWrite, UInt32 StartBlockForWrite, ulong StartWritePosition, ulong WriteFileLength, UInt32 CountNeedBlockForWrite, UInt32 CountBlocksForSystemFiles, out UInt32 Block, out string ErrorMessage)
        {
            ErrorMessage = "";
            UInt32 NumDataBlockOnAccount = 0;
            int CountByteWhichWrite = 0, CountByteNeedWrite = 0;
            /*----------------------\ФАЙЛ ТОЛЬКО СОЗДАЁТСЯ/----------------------*/
            if (CountNeedBlockForWrite == 0)
            {
                Block = 0;
                CountNeedBlockForWrite = Convert.ToUInt32(Math.Floor(Convert.ToDouble(ArrayForWrite.Length) / BootBlockSize));
                if (CountNullBlockForWriteAndCopy(CountNeedBlockForWrite, null) >= CountNeedBlockForWrite)
                {
                    UInt32 NextBlock = 0;
                    FileStream fs = new FileStream(Path_To_File, FileMode.Open);
                    if (ArrayForWrite.Length == 0)
                        NumDataBlockOnAccount = FindNullBlock(CountBlocksForSystemFiles, fs);
                    else
                        while (CountByteWhichWrite < ArrayForWrite.Length)
                        {
                            NumDataBlockOnAccount = NextBlock;
                            if (CountByteWhichWrite == 0)
                            {
                                NumDataBlockOnAccount = FindNullBlock(CountBlocksForSystemFiles, fs);
                                ErrorMessage = NumDataBlockOnAccount.ToString();
                            }
                            else
                            {
                                ChangeStructureASFS(NumDataBlockOnAccount, NextBlock, fs);
                            }
                            NextBlock = FindNullBlock(NumDataBlockOnAccount + 1, fs);
                            if (ArrayForWrite.Length > BootBlockSize)
                                CountByteNeedWrite = Convert.ToInt32(BootBlockSize);
                            else
                                CountByteNeedWrite = ArrayForWrite.Length - CountByteWhichWrite;
                            fs.Position = BootSizeSuperblock + NumDataBlockOnAccount * BootBlockSize;
                            fs.Write(ArrayForWrite, CountByteWhichWrite, CountByteNeedWrite);
                            CountByteWhichWrite += CountByteNeedWrite;
                            ChangeStructureASFS(NumDataBlockOnAccount, EndOfFile, fs);
                        }
                    fs.Close();
                    return true;
                }
                else
                {
                    ErrorMessage = "Недостаточно места!";
                    return false;
                }
            }
            /*----------------------\ЗАПИСЬ В СУЩЕСТВУЮЩИЙ ФАЙЛ/----------------------*/
            Block = 0;
            UInt32 LastBlockForWrite = 0, Position = 0;
            int NeedAreaToWrite = 0;
            if (WriteFileLength - StartWritePosition - Convert.ToUInt64(ArrayForWrite.LongLength) < 0)
                NeedAreaToWrite = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(WriteFileLength - StartWritePosition) / BootBlockSize));
            else
                NeedAreaToWrite = ArrayForWrite.Length;
            if (ArrayForWrite.Length != NeedAreaToWrite)
            {
                if (FindNullBlock(CountBlocksForSystemFiles, null) < NeedAreaToWrite)
                {
                    ErrorMessage = "Недостаточно места!";
                    return false;
                }
            }
            FileStream fs2 = new FileStream(Path_To_File, FileMode.Open);
            while (CountByteWhichWrite != ArrayForWrite.Length)
            {
                Position = SearchNeedBlock(StartBlockForWrite, Convert.ToUInt32(StartWritePosition), out NumDataBlockOnAccount, out LastBlockForWrite, fs2);
                if (Position == EndOfFile)
                {
                    if (BootBlockSize * (NumDataBlockOnAccount + 1) < StartWritePosition + Convert.ToUInt64(ArrayForWrite.Length))
                    {
                        Position = FindNullBlock(CountBlocksForSystemFiles, fs2);
                        if (Position == EndOfFile)
                        {
                            ErrorMessage = "Недостаточно места!";
                            return false;
                        }
                        ChangeStructureASFS(LastBlockForWrite, Position, fs2);
                        ChangeStructureASFS(Position, EndOfFile, fs2);
                        NumDataBlockOnAccount++;
                        Block++;
                    }
                    else
                    {
                        if (BootBlockSize * (NumDataBlockOnAccount + 1) - BootBlockSize >= WriteFileLength && StartWritePosition + Convert.ToUInt64(ArrayForWrite.Length) >= WriteFileLength)
                        {
                            Block = UInt32.MaxValue;
                            ChangeStructureASFS(LastBlockForWrite, FreeSector, fs2);
                            ChangeStructureASFS(SearchNeedBlock(StartBlockForWrite, Convert.ToUInt32((NumDataBlockOnAccount + 1) * Size - 2), out NumDataBlockOnAccount, out LastBlockForWrite, fs2), EndOfFile, fs2);
                        }
                        else
                            Position = LastBlockForWrite;
                    }
                }
                ulong Offset = StartWritePosition - NumDataBlockOnAccount * BootBlockSize;
                if (Offset + Convert.ToUInt64(ArrayForWrite.LongLength) - Convert.ToUInt64(CountByteWhichWrite) > BootBlockSize)
                {
                    CountByteNeedWrite = Convert.ToInt32(BootBlockSize - Offset);
                    StartWritePosition += Convert.ToUInt32(CountByteNeedWrite);
                }
                else
                    CountByteNeedWrite = Convert.ToInt32(ArrayForWrite.Length - CountByteWhichWrite);

                fs2.Position = Convert.ToInt64(Convert.ToUInt64(BootSizeSuperblock) + Convert.ToUInt64(Position * BootBlockSize) + Offset);
                fs2.Write(ArrayForWrite, CountByteWhichWrite, CountByteNeedWrite);
                CountByteWhichWrite += CountByteNeedWrite;
            }
            fs2.Close();
            return true;
        }
        #region Функции необходимые для чтения и записи
        public void ChangeStructureASFS(UInt32 FSBlock, UInt32 NextBlock, FileStream fs)
        {
            UInt32 FSNumBlock = SearchNextBlock(FSBlock, out int CountNeedBlock);
            fs.Position = BootSizeSuperblock + FSNumBlock * BootBlockSize + (FSBlock - CountRecInOneBlock * CountNeedBlock) * Size;
            byte[] ArrayForChangeStruct = FunctionsForFS.GetBytes(new Struct.ASFS() { ValueNextBlock = NextBlock });
            fs.Write(ArrayForChangeStruct, 0, ArrayForChangeStruct.Length);
        }                                       /*При удалении или записи таблица форматируется*/
        public UInt32 SearchNextBlock(UInt32 NumBlock, out int NumSearchBlock)
        {
            NumSearchBlock = 0;
            for (NumSearchBlock = 0; NumSearchBlock < AdressNextBlock.Count; NumSearchBlock++)
            {
                if ((NumBlock + 1) * CountRecInOneBlock > NumBlock)
                    break;
            }
            if (NumBlock == 0)
                return StartFSBlock;
            return AdressNextBlock[Convert.ToInt32(NumSearchBlock)].ValueNextBlock;
        }
        public UInt32 SearchNeedBlock(UInt32 StartBlockFile, UInt32 StartByteFile, out UInt32 NumSearchASFSBlock, out UInt32 LastBlock, FileStream fs)
        {
            LastBlock = 0;
            NumSearchASFSBlock = Convert.ToUInt32(Math.Floor(Convert.ToDouble(StartByteFile) / BootBlockSize));
            UInt32 NumASFSBlock = 0;
            Struct.ASFS asfs = new Struct.ASFS();
            for (UInt32 i = 0; i < NumSearchASFSBlock + 1; i++)
            {
                if (i != 0)
                    StartBlockFile = asfs.ValueNextBlock;
                NumASFSBlock = SearchNextBlock(i, out int SequenceBlocks);
                byte[] byte_array = new byte[Size];
                fs.Position = BootSizeSuperblock + NumASFSBlock * BootBlockSize + (StartBlockFile - CountRecInOneBlock * SequenceBlocks) * Size;
                fs.Read(byte_array, 0, byte_array.Length);
                FunctionsForFS.FromBytes(byte_array, out asfs);
                LastBlock = StartBlockFile;
                if (asfs.ValueNextBlock == EndOfFile)
                    return EndOfFile;
            }
            LastBlock = StartBlockFile;
            return StartBlockFile;
        }
        public UInt32 FindNullBlock(UInt32 SearchStartBlock, FileStream fs)
        {
            byte[] ArrayToConvert;
            if (fs == null)
            {
                for (ulong i = SearchStartBlock * Convert.ToUInt64(Size); i < FSLengthByBytes; i += Convert.ToUInt64(Size))
                {
                    ArrayToConvert = ReadBytes(StartFSBlock, Convert.ToUInt32(i), Convert.ToUInt32(Size), null);
                    FunctionsForFS.FromBytes(ArrayToConvert, out Struct.ASFS asfs);
                    if (asfs.ValueNextBlock == FreeSector)
                        return Convert.ToUInt32(i) / Convert.ToUInt32(Size);
                }
            }
            else
            {
                for (ulong i = SearchStartBlock * Convert.ToUInt64(Size); i < FSLengthByBytes; i += Convert.ToUInt64(Size))
                {
                    ArrayToConvert = ReadBytes(StartFSBlock, Convert.ToUInt32(i), Convert.ToUInt32(Size), fs);
                    FunctionsForFS.FromBytes(ArrayToConvert, out Struct.ASFS asfs);
                    if (asfs.ValueNextBlock == FreeSector)
                        return Convert.ToUInt32(i) / Convert.ToUInt32(Size);
                }
            }
            return EndOfFile;
        }
        public UInt32 CountNullBlockForWriteAndCopy(UInt32 SearchStartBlock, FileStream fs)
        {
            UInt32 CountNullBlock = 0;
            byte[] FirstArray, SecondArray;
            if (fs == null)
            {
                FirstArray = ReadBytes(StartFSBlock, Convert.ToUInt32(FSLengthByBytes - SearchStartBlock * Convert.ToUInt32(Size)), Convert.ToUInt32(SearchStartBlock * Convert.ToUInt64(Size)), null);
                for (long i = 0; i < FirstArray.LongLength; i += Convert.ToInt64(Size))
                {
                    SecondArray = new byte[Size];
                    for (int j = 0; j < Size; j++)
                        SecondArray[j] = FirstArray[i + j];
                    FunctionsForFS.FromBytes(FirstArray, out Struct.ASFS asfs);
                    if (asfs.ValueNextBlock == FreeSector)
                        CountNullBlock++;
                }
            }
            else
            {
                FirstArray = ReadBytes(StartFSBlock, Convert.ToUInt32(FSLengthByBytes - SearchStartBlock * Convert.ToUInt32(Size)), Convert.ToUInt32(SearchStartBlock * Convert.ToUInt64(Size)), fs);
                for (long i = 0; i < FirstArray.LongLength; i += Convert.ToInt64(Size))
                {
                    SecondArray = new byte[Size];
                    for (int j = 0; j < Size; j++)
                        SecondArray[j] = FirstArray[i + j];
                    FunctionsForFS.FromBytes(FirstArray, out Struct.ASFS asfs);
                    if (asfs.ValueNextBlock == FreeSector)
                        CountNullBlock++;
                }
            }
            //fs.Close();
            return CountNullBlock;
        }
        public byte[] ReadFile(UInt32 StartBlock, UInt32 Length, UInt32 StartByte = 0)     /*Чтение файла побайтово*/
        {
            byte[] ReadFileByBytes = new byte[Length];
            using(FileStream fs = new FileStream(Path_To_File, FileMode.Open))
            {
                fs.Position = BootSizeSuperblock + StartBlock * Size + StartByte;
                fs.Read(ReadFileByBytes, 0, Convert.ToInt32(Length));
            };
            return ReadFileByBytes;
        }
        #endregion
    }
}
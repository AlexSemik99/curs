﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace FS.WorkingPart
{
    class BootFS
    {
        public Struct.SuperBlock sb = new Struct.SuperBlock();
        int Size = 0;
        public BootFS()
        {
            Size = Marshal.SizeOf(typeof(FS.Struct.SuperBlock));
        }
        public int SizeStruct
        {
            get
            {
                return Size;
            }
        }
        public UInt32 GetBlockSize()
        {
            return sb.Claster_Size;
        }
        public UInt32 GetCopyMFT()
        {
            return sb.Num_Copy_MFT;
        }
        public void Initialize(UInt32 BlockSize, UInt32 MFT, UInt32 CopyMFT)
        {
            sb.Claster_Size = BlockSize;
            sb.Num_Block = MFT;
            sb.CRC = Convert.ToUInt32(Math.Round(Convert.ToDouble((sb.Claster_Size + sb.Num_Block) / 32)));
            sb.Num_Copy_MFT = CopyMFT;
        }
        public byte[] ByteConvert()
        {
            byte[] b;
            {
                b = FunctionsForFS.GetBytes(sb);
            }
            return b;
        }
        public bool ReadBootSector()
        {
            byte[] record = new byte[Size];
            try
            {
                FileStream fs = new FileStream(ASFS.Path_To_File, FileMode.Open)
                {
                    Position = 0                                                    /*На всякий случай!*/
                };
                fs.Read(record, 0, Size);
                fs.Close();
                FunctionsForFS.FromBytes(record, out sb);
                if (Convert.ToUInt32(Math.Round(Convert.ToDouble((sb.Claster_Size + sb.Num_Block) / 32))) == sb.CRC)
                    return true;
                else
                {
                    fs.Position = new FileInfo(ASFS.Path_To_File).Length - Size;
                    fs.Read(record, 0, Size);
                    fs.Close();
                    if (Convert.ToUInt32(Math.Round(Convert.ToDouble((sb.Claster_Size + sb.Num_Block) / 32))) == sb.CRC)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
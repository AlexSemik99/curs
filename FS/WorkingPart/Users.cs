﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace FS.WorkingPart
{
    class Users
    {
        public Struct.User see_user;
        Struct.MFT mft;
        ASFS asfs;
        List<Struct.User> users = new List<Struct.User>();
        int Size = 0;
        public Users()
        {
            Size = Marshal.SizeOf(typeof(Struct.User));
        }
        public UInt32 GetBlockSize(UInt32 SectorBlock)
        {
            return Convert.ToUInt32(Math.Ceiling(Convert.ToDouble(Size * users.Count) / SectorBlock));
        }
        public int GetSizeBytes()
        {
            return Size * users.Count;
        }
        public bool AddUser(string NameUser, string UserLogin, string UserPassword, UInt32 IDGroup)
        {
            Struct.User us = new Struct.User
            {
                Id_Group = IDGroup
            };
            if (NameUser.Length > 25) us.User_Name = NameUser.Substring(0, 25).ToCharArray();
            else if (NameUser.Length == 25) us.User_Name = NameUser.ToCharArray();
            else
            {
                us.User_Name = new char[25];
                string str = "";
                for (int i = 0; i < 25 - NameUser.Length; i++)
                    str += ' ';
                NameUser.ToCharArray().CopyTo(us.User_Name, 0);
                str.ToCharArray().CopyTo(us.User_Name, NameUser.Length);
            }

            if (UserLogin.Length > 20) us.Login = UserLogin.Substring(0, 20).ToCharArray();
            else if (UserLogin.Length == 20) us.Login = UserLogin.ToCharArray();
            else
            {
                us.Login = new char[20];
                string str = "";
                for (int i = 0; i < 20 - UserLogin.Length; i++)
                    str += ' ';
                UserLogin.ToCharArray().CopyTo(us.Login, 0);
                str.ToCharArray().CopyTo(us.Login, UserLogin.Length);
            }

            us.Hesh_Password = Hashing.Hash(UserPassword, 15);

            us.Home_Directory = new char[32];
            string Start = "/USERS/";
            for (int i = 0; i < Start.Length; i++)
                us.Home_Directory[i] = Start[i];
            for (int i = Start.Length; i < Start.Length + us.Login.Length; i++)
                us.Home_Directory[i] = us.Login[i - Start.Length];
            Struct.User u = users.Find((x) => x.Login == us.Login);
            if (u.Login != null)
                return false;
            us.Id_User = Convert.ToUInt32(users.Count) + 1;
            users.Add(us);
            return true;
        }
        public byte[] Convert_To_byte()
        {
            byte[] bt = new byte[Size * users.Count];
            byte[] b; int k = 0;
            for (int i = 0; i < users.Count; i++)
            {
                b = FunctionsForFS.GetBytes(users[i]);
                b.CopyTo(bt, k);
                k += b.Length;
            }
            return bt;
        }
        public void Initialize(ASFS fs, Struct.MFT MFT)
        {
            asfs = fs;
            mft = MFT;
        }
        public bool CheckUserForExistence(string Login, string Password, out UInt32 ID)
        {
            Struct.User user;
            UInt32 Read = 0; ID = 0;
            char[] Check;
            if (Login.Length > 20) Check = Login.Substring(0, 20).ToCharArray();
            else if (Login.Length == 20) Check = Login.ToCharArray();
            else
            {
                Check = new char[20];
                string str = "";
                for (int i = 0; i < 20 - Login.Length; i++)
                    str += ' ';
                Login.ToCharArray().CopyTo(Check, 0);
                str.ToCharArray().CopyTo(Check, Login.Length);
            }
            byte[] ArrayForCheck;
            while (Read < mft.Busy_Bytes)
            {
                user = new Struct.User();
                ArrayForCheck = new byte[Size];
                ArrayForCheck = asfs.ReadBytes(mft.AdressBlockFile, Read, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ArrayForCheck, out user);
                ID = (ID > user.Id_User) ? ID : user.Id_User;
                if (Password != "")
                {
                    char[] CheckPassword = Hashing.Hash(Password, 15);
                    if (user.Login.SequenceEqual(Check) && user.Hesh_Password.SequenceEqual(CheckPassword))
                        return true;
                }
                else
                    if (user.Login.SequenceEqual(Check))
                    return true;
                Read += Convert.ToUInt32(Size);
            }
            return false;
        }
        public bool LoginInSystem(string UserLogin, string UserPassword)
        {
            UInt32 Read = 0;
            char[] Check, CheckPassword = Hashing.Hash(UserPassword, 15);
            if (UserLogin.Length > 20) Check = UserLogin.Substring(0, 20).ToCharArray();
            else if (UserLogin.Length == 20) Check = UserLogin.ToCharArray();
            else
            {
                Check = new char[20];
                string str = "";
                for (int i = 0; i < 20 - UserLogin.Length; i++)
                    str += ' ';
                UserLogin.ToCharArray().CopyTo(Check, 0);
                str.ToCharArray().CopyTo(Check, UserLogin.Length);
            }
            byte[] ArrayForCheck;
            while (Read < mft.Busy_Bytes)
            {
                see_user = new Struct.User();
                ArrayForCheck = new byte[Size];
                ArrayForCheck = asfs.ReadBytes(mft.AdressBlockFile, Read, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ArrayForCheck, out see_user);
                if (see_user.Hesh_Password.SequenceEqual(CheckPassword) && see_user.Login.SequenceEqual(Check)) 
                    return true;
                Read += Convert.ToUInt32(Size);
            }
            return false;
        }                   /*Проверка пользователя при входе в систему*/
        public bool CreateUser(Groups group, MFT M, UInt32 CopyMFT)
        {
            UInt32 UserID = 0, GroupID = 0;
            Console.Write("Имя: ");
            string UserName = Console.ReadLine();
            Console.Write("Логин: ");
            string UserLogin = Console.ReadLine();
            while (CheckUserForExistence(UserLogin, "", out UserID))
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Такой логин уже занят!");
                Console.ResetColor();
                Console.Write("Логин: ");
                UserLogin = Console.ReadLine();
            }
            Console.Write("Пароль: ");
            string UserPassword = Console.ReadLine();
            var WrGroups = group.GetGroups();
            foreach (var s in WrGroups)
            {
                Console.WriteLine($"{s.Id_Group}{new string(s.Name_Group),25}{s.Access_Level}");
            }
            //Console.Write("Группа: ");
            //bool ExitCycle = false;
            //Groups groups = new Groups();
            //while (!ExitCycle)
            //{
            //    while (!UInt32.TryParse(Console.ReadLine(), out GroupID))
            //    {
            //        Console.Clear();
            //        Console.WriteLine("Неверное значение!");
            //    }

            //    for (int i = 0; i < WrGroups.Length; i++)
            //    {
            //        if (GroupID == WrGroups[i].Id_Group)
            //        {
            //            ExitCycle = true;
            //            break;
            //        }
            //    }
            //}
            Struct.User us = new Struct.User
            {
                Id_Group = 2
            };
            if (UserName.Length > 25) us.User_Name = UserName.Substring(0, 25).ToCharArray();
            else if (UserName.Length == 25) us.User_Name = UserName.ToCharArray();
            else
            {
                us.User_Name = new char[25];
                string str = "";
                for (int i = 0; i < 25 - UserName.Length; i++)
                    str += ' ';
                UserName.ToCharArray().CopyTo(us.User_Name, 0);
                str.ToCharArray().CopyTo(us.User_Name, UserName.Length);
            }

            if (UserLogin.Length > 20) us.Login = UserLogin.Substring(0, 20).ToCharArray();
            else if (UserLogin.Length == 20) us.Login = UserLogin.ToCharArray();
            else
            {
                us.Login = new char[20];
                string str = "";
                for (int i = 0; i < 20 - UserLogin.Length; i++)
                    str += ' ';
                UserLogin.ToCharArray().CopyTo(us.Login, 0);
                str.ToCharArray().CopyTo(us.Login, UserLogin.Length);
            }
            us.Hesh_Password = Hashing.Hash(UserPassword, 15); 
            us.Home_Directory = new char[27];
            string Start = "/USERS/";
            for (int i = 0; i < Start.Length; i++)
                us.Home_Directory[i] = Start[i];
            for (int i = Start.Length; i < Start.Length + us.Login.Length; i++)
                us.Home_Directory[i] = us.Login[i - Start.Length];
            us.Id_User = UserID + 1;
            byte[] ArrayForCreate = FunctionsForFS.GetBytes(us);
            if (!asfs.WriteFileByBytes(ArrayForCreate, mft.AdressBlockFile, mft.Busy_Bytes, Convert.ToUInt64(ArrayForCreate.Length), mft.Number_Allocated_Blocks, 0, out uint AddBlock, out string ErrorMessage))
                return false;
            mft.Number_Allocated_Blocks += AddBlock;
            mft.Busy_Bytes += Convert.ToUInt32(ArrayForCreate.Length);
            byte[] CreateContent = FunctionsForFS.GetBytes(mft);
            var _mft_r = M.SearchSystemInformationInMFT("MFT");
            asfs.WriteFileByBytes(CreateContent, _mft_r.AdressBlockFile, Convert.ToUInt32(M.GetSizeRecord(Convert.ToInt32(mft.Record_Number - 1), 0)), _mft_r.Busy_Bytes, 1, 0, out AddBlock, out ErrorMessage);
            asfs.WriteFileByBytes(CreateContent, CopyMFT, Convert.ToUInt32(M.GetSizeRecord(Convert.ToInt32(mft.Record_Number - 1), 0)), _mft_r.Busy_Bytes, 1, 0, out AddBlock, out ErrorMessage);
            M.GetAllFilesInDirectory("/USERS/", out UInt32 LastID);
            M.MakeDirectory(UserLogin, "/USERS/", us.Id_User);
            return true;
        }
        public bool RemoveUser(string Login, string Password, UInt32 CopyMFT, MFT m)
        {
            if (!CheckUserForExistence(Login, Password, out UInt32 IDSearchUser))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Пользователя с такими параметрами не существует в системе!");
                Console.ResetColor();
                return false;
            }
            if (m.CheckPathOnExist("/Users/" + Login))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Пользовательской папки нет в системе!");
                Console.ResetColor();
                return false;
            }
            UInt32 CountReadValues = (IDSearchUser - 1) * Convert.ToUInt32(Size);
            byte[] Read;
            string ErrorMessage;
            do
            {
                CountReadValues += Convert.ToUInt32(Size);
                Read = new byte[Size];
                Read = asfs.ReadBytes(mft.AdressBlockFile, CountReadValues, Convert.ToUInt32(Size), null);
                asfs.WriteFileByBytes(Read, 0, CountReadValues - Convert.ToUInt32(Size), mft.Busy_Bytes - Convert.ToUInt32(Read.Length), mft.Number_Allocated_Blocks, 0, out uint block, out ErrorMessage);
            } while (CountReadValues < mft.Busy_Bytes);
            mft.Busy_Bytes -= Convert.ToUInt32(Size);
            mft.Number_Allocated_Blocks = Convert.ToUInt32(Math.Ceiling(Convert.ToDouble(mft.Busy_Bytes) / asfs.BootBlockSize));
            byte[] Content = FunctionsForFS.GetBytes(mft);
            var mf = m.SearchSystemInformationInMFT("MFT");
            asfs.WriteFileByBytes(Content, mf.AdressBlockFile, Convert.ToUInt32(mft.UserID * m.GetSizeRecord(1, 0)), Convert.ToUInt32(Content.Length), 1, 0, out uint AddBlock, out ErrorMessage);
            asfs.WriteFileByBytes(Content, CopyMFT, Convert.ToUInt32(mft.UserID * m.GetSizeRecord(1, 0)), Convert.ToUInt32(Content.Length), 1, 0, out AddBlock, out ErrorMessage);
            return true;
        }
        public List<Struct.User> GetUsers()
        {
            List<Struct.User> ls = new List<Struct.User>();
            uint count_read = 0; byte[] Read;

            //чтение файла и поиск пользователя
            while (count_read < mft.Busy_Bytes)
            {
                var users = new Struct.User();
                Read = new byte[Size];
                Read = asfs.ReadBytes(mft.AdressBlockFile, count_read, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(Read, out users);
                ls.Add(users);
                count_read += Convert.ToUInt32(Size);
            }
            return ls;
        }
    }
}
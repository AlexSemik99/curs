﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;

namespace FS.WorkingPart
{
    class MFT
    {
        List<Struct.MFT> mft = new List<Struct.MFT>();
        int Size = 0;
        UInt32 CopyMFTBlock, UsingMFTBlock;
        ASFS ASFS;
        public Groups _groups;
        public Users _users;
        #region Запись данных в MFT
        public MFT() => Size = Marshal.SizeOf(typeof(Struct.MFT));
        public int GetSizeBytes()
        {
            return Size * mft.Count;
        }
        public int GetSizeRecord(int count, UInt32 claster)
        {
            if (claster == 0)
                return Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Size * count)));
            else
                return Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Size * count) / Convert.ToDouble(claster)));
        }
        public bool AddMFTRecord(UInt32 Number_Allocated_Blocks1, string FileName, UInt32 Busy_Bytes, UInt32 Base_Record_Number, UInt32 UserID, string File_Attributes, DateTime Create_File, string Access_Level, string type, UInt32 AdressBlockFile)
        {
            Struct.MFT mft_rec = new Struct.MFT
            {
                Number_Allocated_Blocks = Number_Allocated_Blocks1,
                Access_Level = Access_Level.ToCharArray(),
                AdressBlockFile = AdressBlockFile,
                Base_Record_Number = Base_Record_Number,
                Busy_Bytes = Busy_Bytes,
                File_Attributes = File_Attributes.ToCharArray(),
                UserID = UserID,
                Create_File = Create_File.ToBinary(),
                Record_Number = Convert.ToUInt32(mft.Count + 1),
                File_Type = Convert.ToChar(type)
            };
            if (FileName.Length > 25)
                mft_rec.File_Name = FileName.Substring(0, 25).ToCharArray();
            else if (FileName.Length == 25)
                mft_rec.File_Name = FileName.ToCharArray();
            else
            {
                mft_rec.File_Name = new char[25];
                string tmp = "";
                for (int i = 0; i < 25 - FileName.Length; i++)
                    tmp += ' ';
                FileName.ToCharArray().CopyTo(mft_rec.File_Name, 0);
                tmp.ToCharArray().CopyTo(mft_rec.File_Name, FileName.Length);
            }
            Struct.MFT k = mft.Find((x) => x.Base_Record_Number == mft_rec.Base_Record_Number && x.File_Name == mft_rec.File_Name && x.File_Attributes == mft_rec.File_Attributes);
            if (k.File_Name != null)
                return false;
            mft.Add(mft_rec);
            return true;
        }
        public byte[] Convert_To_byte()
        {
            byte[] bt = new byte[Size * mft.Count];
            int k = 0;
            byte[] b;
            for (int i = 0; i < mft.Count; i++)
            {
                b = FunctionsForFS.GetBytes(mft[i]);
                b.CopyTo(bt, k);
                k += b.Length;
            }
            return bt;
        }
        #endregion
        public void Initialize(UInt32 CopyMFT, UInt32 MFTBlock, ASFS asfs)
        {
            CopyMFTBlock = CopyMFT;
            UsingMFTBlock = MFTBlock;
            ASFS = asfs;
        }
        public Struct.MFT GetFS()
        {
            byte[] Read = ASFS.ReadFile(UsingMFTBlock, Convert.ToUInt32(Size), Convert.ToUInt32(Size));
            FunctionsForFS.FromBytes(Read, out Struct.MFT m);
            return m;
        }
        public Struct.MFT SearchSystemInformationInMFT(string FileName)
        {
            char[] File = new char[25];
            if (FileName.Length > 25) File = FileName.Substring(0, 25).ToCharArray();
            else if (FileName.Length == 25) File = FileName.ToCharArray();
            else
            {
                string str = "";

                for (int i = 0; i < 25 - FileName.Length; i++) str += ' ';
                FileName.ToCharArray().CopyTo(File, 0);
                str.ToCharArray().CopyTo(File, FileName.Length);
            }
            Struct.MFT ReadMFT = new Struct.MFT();
            Struct.MFT Value = new Struct.MFT();
            byte[] ArrayForRead;
            ArrayForRead = ASFS.ReadFile(UsingMFTBlock, Convert.ToUInt32(Size));
            FunctionsForFS.FromBytes(ArrayForRead, out ReadMFT);
            if (File.SequenceEqual(ReadMFT.File_Name))
                return ReadMFT;
            for (int i = Size; i < ReadMFT.Busy_Bytes; i += Size)
            {
                ArrayForRead = ASFS.ReadBytes(ReadMFT.AdressBlockFile, Convert.ToUInt32(i), Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ArrayForRead, out Value);
                if (File.SequenceEqual(Value.File_Name))
                    return Value;
            }
            return new Struct.MFT();
        }
        public Struct.MFT GetFileInformation(string FileName, UInt32 IDParent, string DirctoryCheck)
        {
            char[] File = new char[25];
            if (FileName.Length > 25) File = FileName.Substring(0, 25).ToCharArray();
            else if (FileName.Length == 25) File = FileName.ToCharArray();
            else
            {
                string tmp = "";
                for (int i = 0; i < 25 - FileName.Length; i++)
                    tmp += ' ';
                FileName.ToCharArray().CopyTo(File, 0);
                tmp.ToCharArray().CopyTo(File, FileName.Length);
            }
            var _mft = SearchSystemInformationInMFT("MFT");
            UInt32 CountReadBytes = 0;
            byte[] ArrayForRead;
            while (CountReadBytes < _mft.Busy_Bytes)
            {
                var MFT = new Struct.MFT();
                ArrayForRead = new byte[Size];
                ArrayForRead = ASFS.ReadBytes(MFT.AdressBlockFile, CountReadBytes, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ArrayForRead, out MFT);
                if (MFT.Base_Record_Number == IDParent && MFT.File_Name.SequenceEqual(File))
                {
                    if (DirctoryCheck == "-") 
                    {
                        if (MFT.File_Type == 'd')
                            return MFT;
                    }
                    else
                        return MFT;
                }
                CountReadBytes += Convert.ToUInt32(Size);
            }
            return new Struct.MFT();
        }                  /*Информация о директории или о файле*/
        public bool CheckPathOnExist(string Path)
        {
            UInt32 ID = SearchSystemInformationInMFT(".").Record_Number;
            string[] ElementsInPath = Path.Split('/');
            for(int i = 1; i < ElementsInPath.Length; i++)
            {
                if (ElementsInPath[i] == "")
                    continue;
                var FileInf = GetFileInformation(ElementsInPath[i], ID, "-");
                if (FileInf.File_Name == null | FileInf.File_Type == '-')
                    return false;
                ID = FileInf.Record_Number;
            }
            return true;
        }
        public bool FileInDirectory(string PathToFile, string FileName, out char[] Result)
        {
            if (FileName.Length > 25) Result = FileName.Substring(0, 25).ToCharArray();
            else if (FileName.Length == 25) Result = FileName.ToCharArray();
            else
            {
                Result = new char[25];
                string tmp = "";

                for (int i = 0; i < 25 - FileName.Length; i++) tmp += ' ';
                FileName.ToCharArray().CopyTo(Result, 0);
                tmp.ToCharArray().CopyTo(Result, FileName.Length);
            }
            UInt32 ID = GetIDForCurrentPath(PathToFile);
            var MFTInf = SearchSystemInformationInMFT("MFT");
            UInt32 CountReadByte = 0;
            byte[] ReadByte;
            while (CountReadByte < MFTInf.Busy_Bytes)
            {
                var _mft = new Struct.MFT();
                ReadByte = new byte[Size];
                ReadByte = ASFS.ReadBytes(MFTInf.AdressBlockFile, CountReadByte, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ReadByte, out _mft);
                if (_mft.Base_Record_Number == ID && _mft.File_Type == '-' && _mft.File_Name.SequenceEqual(Result))
                    return true;
                CountReadByte += Convert.ToUInt32(Size);
            }
            return false;
        }
        public bool ExistFileInDirectory(string PathToFile, string FileName, out Struct.MFT _mFT)
        {
            char[] Result;
            if (FileName.Length > 25) Result = FileName.Substring(0, 25).ToCharArray();
            else if (FileName.Length == 25) Result = FileName.ToCharArray();
            else
            {
                Result = new char[25];
                string tmp = "";

                for (int i = 0; i < 25 - FileName.Length; i++) tmp += ' ';
                FileName.ToCharArray().CopyTo(Result, 0);
                tmp.ToCharArray().CopyTo(Result, FileName.Length);
            }
            UInt32 ID = GetIDForCurrentPath(PathToFile);
            var MFTInf = SearchSystemInformationInMFT("MFT");
            UInt32 CountReadByte = 0;
            byte[] ReadByte;
            while (CountReadByte < MFTInf.Busy_Bytes)
            {
                _mFT = new Struct.MFT();
                ReadByte = new byte[Size];
                ReadByte = ASFS.ReadBytes(MFTInf.AdressBlockFile, CountReadByte, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ReadByte, out _mFT);
                if (_mFT.Base_Record_Number == ID && _mFT.File_Type == '-' && _mFT.File_Name.SequenceEqual(Result))
                    return true;
                CountReadByte += Convert.ToUInt32(Size);
            }
            _mFT = new Struct.MFT();        /*Если ничего не нашли*/
            return false;
        }
        public UInt32 GetIDForCurrentPath(string Path)
        {
            UInt32 ID = SearchSystemInformationInMFT(".").Record_Number;
            string[] Elements = Path.Split('/');
            for (int i = 1; i < Elements.Length - 1; i++)
            {
                if (Elements[i] == "")
                    continue;
                ID = GetFileInformation(Elements[i], ID, "-").Record_Number;
            }
            return ID;
        }
        public UInt32 GetIDForCurrentPath(string Path, out Struct.MFT RetMFT)
        {
            UInt32 ID = SearchSystemInformationInMFT(".").Record_Number;
            RetMFT = new Struct.MFT();
            string[] Elements = Path.Split('/');
            for (int i = 1; i < Elements.Length - 1; i++)
            {
                if (Elements[i] == "")
                    continue;
                RetMFT = GetFileInformation(Elements[i], ID, "-");
                ID = RetMFT.Record_Number;
            }
            return ID;
        }
        public Struct.MFT InfoFile(string FileName, UInt32 BaseRecord, string CheckDirectory)
        {
            char[] Read = new char[25];
            if (FileName.Length > 25) Read = FileName.Substring(0, 25).ToCharArray();
            else if (FileName.Length == 25) Read = FileName.ToCharArray();
            else
            {
                string TMP = "";
                for (int i = 0; i < 25 - FileName.Length; i++) TMP += ' ';
                FileName.ToCharArray().CopyTo(Read, 0);
                TMP.ToCharArray().CopyTo(Read, FileName.Length);
            }

            var mft = SearchSystemInformationInMFT("MFT");
            uint count_read = 0; byte[] ArrayForRead;
            while (count_read < mft.Busy_Bytes)
            {
                var m = new Struct.MFT();
                ArrayForRead = new byte[Size];
                ArrayForRead = ASFS.ReadBytes(mft.AdressBlockFile, count_read, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ArrayForRead, out m);
                if (m.Base_Record_Number == BaseRecord && m.File_Name.SequenceEqual(Read))
                {
                    if (CheckDirectory == "-") 
                    {
                        if (m.File_Type == 'd')
                            return m;
                    }
                    else
                        return m;
                }
                count_read += Convert.ToUInt32(Size);
            }
            return new Struct.MFT();
        }
        public List<Struct.MFT> GetAllFilesInDirectory(string PathToDirectory, out UInt32 LastId)
        {
            List<Struct.MFT> FilesInDirectory = new List<Struct.MFT>();
            UInt32 ID = GetIDForCurrentPath(PathToDirectory);
            var _MFT = SearchSystemInformationInMFT("MFT");
            UInt32 ReadByteOfInfo = 0;
            byte[] ReadBytes;
            LastId = 0;
            while (ReadByteOfInfo < _MFT.Busy_Bytes)
            {
                var m = new Struct.MFT();
                ReadBytes = new byte[Size];
                ReadBytes = ASFS.ReadBytes(_MFT.AdressBlockFile, ReadByteOfInfo, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ReadBytes, out m);
                if (m.Base_Record_Number == ID)
                    FilesInDirectory.Add(m);
                 LastId = (m.Record_Number > LastId) ? m.Record_Number : LastId;
                ReadByteOfInfo += Convert.ToUInt32(Size);
            }
            return FilesInDirectory;
        }
        #region Комманды
        public void PrintFiles(string PathToFile, UInt32 UserID)
        {
            List<Struct.MFT> _INFO = GetAllFilesInDirectory(PathToFile, out UInt32 Last);
            foreach (Struct.MFT _mft in _INFO)
            {
                if (_mft.UserID == UserID)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{_mft.File_Type}{new string(_mft.Access_Level),5} {new string(_mft.File_Name).Trim(' '),5} {_mft.Busy_Bytes,5} {_mft.Number_Allocated_Blocks * ASFS.BootBlockSize,5} {DateTime.FromBinary(_mft.Create_File).ToString(),20}");
                    Console.ResetColor();
                }
                else if(UserID == 1)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"{_mft.File_Type}{new string(_mft.Access_Level),5} {new string(_mft.File_Name).Trim(' '),5} {_mft.Busy_Bytes,5} {_mft.Number_Allocated_Blocks * ASFS.BootBlockSize,5} {DateTime.FromBinary(_mft.Create_File).ToString(),20}");
                    Console.ResetColor();
                }
            }
        }                                                      /*ls*/
        public bool MakeFile(string Path, byte[] value, string AttributeFile, string Access, UInt32 IDUser)
        {
            string[] Elements = Path.Split('/');
            if (Elements[Elements.Length - 1] == "")
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Имя файла не введено");
                Console.ResetColor();
                return false;
            }
            if (value == null | value.Length == 0)
                value = new byte[0];
            if (FileInDirectory(Path.Remove(Path.Length - Elements[Elements.Length - 1].Length), Elements[Elements.Length - 1], out char[] Result))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Такой файл уже существует!");
                Console.ResetColor();
                return false;
            }
            if (ASFS.WriteFileByBytes(value, 0, 0, 0, 0, CopyMFTBlock * 2 / 100 * 12, out UInt32 Block, out string MessageError))
            {
                GetAllFilesInDirectory(Path, out UInt32 t);
                Struct.MFT mFT = new Struct.MFT
                {
                    AdressBlockFile = Convert.ToUInt32(MessageError),
                    Create_File = DateTime.Now.ToBinary(),
                    File_Attributes = AttributeFile.ToCharArray(),
                    UserID = IDUser,
                    Access_Level = Access.ToCharArray(),
                    Base_Record_Number = GetIDForCurrentPath(Path.Substring(0, Path.Length - Elements[Elements.Length - 1].Length)),
                    File_Type = '-',
                    Busy_Bytes = Convert.ToUInt32(value.Length),
                    Number_Allocated_Blocks = Convert.ToUInt32(Math.Ceiling(Convert.ToDouble(value.Length) / Convert.ToDouble(ASFS.BootBlockSize))),
                    Record_Number = t + 1
                };
                if (Elements[Elements.Length - 1].Length > 25) mFT.File_Name = Elements[Elements.Length - 1].Substring(0, 25).ToCharArray();
                else if (Elements[Elements.Length - 1].Length == 25) mFT.File_Name = Elements[Elements.Length - 1].ToCharArray();
                else
                {
                    mFT.File_Name = new char[25];
                    string tmp = "";

                    for (int i = 0; i < 25 - Elements[Elements.Length - 1].Length; i++) tmp += ' ';
                    Elements[Elements.Length - 1].ToCharArray().CopyTo(mFT.File_Name, 0);
                    tmp.ToCharArray().CopyTo(mFT.File_Name, Elements[Elements.Length - 1].Length);
                }
                byte[] ReadByte = FunctionsForFS.GetBytes(mFT);
                var MFTRecord = SearchSystemInformationInMFT("MFT");
                if (!ASFS.WriteFileByBytes(ReadByte, MFTRecord.AdressBlockFile, MFTRecord.Busy_Bytes, MFTRecord.Busy_Bytes, MFTRecord.Number_Allocated_Blocks, 0, out UInt32 AddBlock, out MessageError))
                    return false;
                if (AddBlock != 0)
                    MFTRecord.Number_Allocated_Blocks += AddBlock;
                MFTRecord.Busy_Bytes += Convert.ToUInt32(ReadByte.Length);
                byte[] ReadByte1 = FunctionsForFS.GetBytes(MFTRecord);
                ASFS.WriteFileByBytes(ReadByte1, MFTRecord.AdressBlockFile, 0, Convert.ToUInt64(ReadByte.Length), 1, 0, out AddBlock, out MessageError);
                ASFS.WriteFileByBytes(ReadByte1, CopyMFTBlock, 0, Convert.ToUInt64(ReadByte.Length), 1, 0, out AddBlock, out MessageError);
                return true;
            }
            else
                Console.WriteLine(MessageError);
            return false;
        }           /*cat >*/
        public bool RemoveFile(string NameFile, string Path, UInt32 UserID)
        {
            char[] Value;
            string temp = "";
            bool Find = false;
            if (NameFile.Length > 25)
                Value = NameFile.Substring(0, 25).ToCharArray();
            else if (NameFile.Length == 25)
                Value = NameFile.ToCharArray();
            else
            {
                Value = new char[25];
                for (int i = 0; i < 25 - NameFile.Length; i++)
                    temp += ' ';
                NameFile.ToCharArray().CopyTo(Value, 0);
                temp.ToCharArray().CopyTo(Value, NameFile.Length);
            }
            var _INFO = GetAllFilesInDirectory(Path, out UInt32 Last);
            Struct.MFT m = new Struct.MFT();
            foreach (var Col in _INFO)
            {
                if (Col.File_Type == '-')
                {
                    if (Col.File_Name.SequenceEqual(Value))
                    {
                        m = Col;
                        Find = true;
                        break;
                    }
                }
            }
            if (!Find)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Файла не существует!");
                Console.ResetColor();
                return false;
            }
            var _InfoMFT = SearchSystemInformationInMFT("MFT");
            UInt32 ReadBytes = 0;
            byte[] ReadArray;
            Struct.MFT _mFT = new Struct.MFT();                 /*Пришлось объявить так из-за возможной ошибки в 397 строке*/
            while (ReadBytes < _InfoMFT.Busy_Bytes)
            {
                _mFT = new Struct.MFT();
                ReadArray = new byte[Size];
                ReadArray = ASFS.ReadBytes(_mFT.AdressBlockFile, ReadBytes, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ReadArray, out _mFT);
                if (_mFT.Base_Record_Number == m.Base_Record_Number && _mFT.File_Type == '-' && _mFT.File_Name.SequenceEqual(m.File_Name))
                {
                    break;
                }
                ReadBytes += Convert.ToUInt32(Size);
            }
            if (_mFT.AdressBlockFile == 0)
                return false;
            ASFS.DeleteFile(_mFT.AdressBlockFile);
            UInt32 Block;
            string ErrorMessage = "";
            do
            {
                ReadBytes += Convert.ToUInt32(Size);
                ReadArray = new byte[Size];
                ReadArray = ASFS.ReadBytes(_InfoMFT.AdressBlockFile, ReadBytes, Convert.ToUInt32(Size), null);
                ASFS.WriteFileByBytes(ReadArray, 0, ReadBytes - Convert.ToUInt32(Size), _InfoMFT.Busy_Bytes - Convert.ToUInt64(ReadArray.LongLength), _InfoMFT.Number_Allocated_Blocks, 0, out Block, out ErrorMessage);
            } while (ReadBytes < _InfoMFT.Busy_Bytes);
            _InfoMFT.Busy_Bytes -= Convert.ToUInt32(ReadArray.Length);
            byte[] b1 = FunctionsForFS.GetBytes(_InfoMFT);
            ASFS.WriteFileByBytes(b1, _InfoMFT.AdressBlockFile, 0, Convert.ToUInt64(ReadArray.Length), 1, 0, out Block, out ErrorMessage);
            ASFS.WriteFileByBytes(b1, CopyMFTBlock, 0, Convert.ToUInt32(ReadArray.Length), 1, 0, out Block, out ErrorMessage);
            return true;
        }                                           /*rm*/
        public bool MakeDirectory(string NameDirectory, string CurrentPath, UInt32 IDUser)
        {
            char[] Value;
            if (NameDirectory.Length > 25)
                Value = NameDirectory.Substring(0, 25).ToCharArray();
            else if (NameDirectory.Length == 25)
                Value = NameDirectory.ToCharArray();
            else
            {
                Value = new char[25];
                string tmp = "";

                for (int i = 0; i < 25 - NameDirectory.Length; i++)
                    tmp += ' ';
                NameDirectory.ToCharArray().CopyTo(Value, 0);
                tmp.ToCharArray().CopyTo(Value, NameDirectory.Length);
            }
            var _INFO = GetAllFilesInDirectory(CurrentPath, out UInt32 Last);
            foreach (var Inf in _INFO)
            {
                if (Inf.File_Type == 'd' && Inf.File_Name.SequenceEqual(Value))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Такой каталог уже существует.");
                    Console.ResetColor();
                    return false;
                }
            }
            var MFTRecords = SearchSystemInformationInMFT("MFT");
            var IDParent = GetIDForCurrentPath(CurrentPath);
            Struct.MFT mFT = new Struct.MFT
            {
                Access_Level = "rwxrwx---".ToCharArray(),
                AdressBlockFile = 0,
                Base_Record_Number = IDParent,
                Busy_Bytes = 0,
                Create_File = DateTime.Now.ToBinary(),
                File_Attributes = "---".ToCharArray(),
                File_Name = Value,
                File_Type = 'd',
                Number_Allocated_Blocks = 0,
                Record_Number = Last + 1,
                UserID = IDUser
            };
            byte[] ArrayForCreateDir = FunctionsForFS.GetBytes(mFT);
            if (!ASFS.WriteFileByBytes(ArrayForCreateDir, MFTRecords.AdressBlockFile, MFTRecords.Busy_Bytes, MFTRecords.Busy_Bytes, MFTRecords.Number_Allocated_Blocks, 0, out UInt32 Block, out string ErrorMessage))
                return false;
            if (Block != 0)
                MFTRecords.Number_Allocated_Blocks += Block;
            MFTRecords.Busy_Bytes += Convert.ToUInt32(ArrayForCreateDir.Length);
            byte[] ArrayForCreateDir2 = FunctionsForFS.GetBytes(MFTRecords);
            ASFS.WriteFileByBytes(ArrayForCreateDir2, MFTRecords.AdressBlockFile, 0, Convert.ToUInt64(ArrayForCreateDir.Length), 1, 0, out Block, out ErrorMessage);
            ASFS.WriteFileByBytes(ArrayForCreateDir2, CopyMFTBlock, 0, Convert.ToUInt64(ArrayForCreateDir.Length), 1, 0, out Block, out ErrorMessage);
            return true;
        }                            /*mkdir*/
        public bool MoveDirectory(string FromWhere, string Where, UInt32 USERID, bool CHID)
        {
            if (CheckPathOnExist(FromWhere))
            {
                if (!CheckPathOnExist(Where))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Заданного нового пути не существует!");
                    Console.ResetColor();
                    return false;
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Каталога не существует по заданному пути!");
                Console.ResetColor();
                return false;
            }
            char[] k;
            if ("USERS".Length > 25) k = "USERS".Substring(0, 25).ToCharArray();
            else if ("USERS".Length == 25) k = "USERS".ToCharArray();
            else
            {
                k = new char[25];
                string tmp = "";
                for (int i = 0; i < 25 - "USERS".Length; i++) tmp += ' ';
                "USERS".ToCharArray().CopyTo(k, 0);
                tmp.ToCharArray().CopyTo(k, "USERS".Length);
            }
            UInt32 RN = GetIDForCurrentPath(FromWhere, out Struct.MFT mf_S);
            if (mf_S.File_Name.SequenceEqual(k))
            {
                Console.WriteLine("Это системная папка. Ее нельзя создавать или удалять");
                return false;
            }
            UInt32 RN2 = GetIDForCurrentPath(Where, out Struct.MFT mf_D);
            if (mf_D.File_Name.SequenceEqual(k))
            {
                Console.WriteLine("Это системная папка.В нее нельзя копировать");
                return false;
            }
            var mf_t = SearchSystemInformationInMFT("MFT");
            UInt32 ReadByte = 0;
            byte[] ReadArray;
            while (ReadByte < mf_t.Busy_Bytes)
            {
                var m = new Struct.MFT();
                ReadArray = new byte[Size];
                ReadArray = ASFS.ReadBytes(mf_t.AdressBlockFile, ReadByte, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ReadArray, out m);
                if (m.Record_Number == mf_S.Record_Number)
                {
                    m.Base_Record_Number = mf_D.Record_Number;
                    if (CHID)
                        m.UserID = USERID;
                    ReadArray = new byte[Size];
                    ReadArray = FunctionsForFS.GetBytes(m);
                    ASFS.WriteFileByBytes(ReadArray, mf_t.AdressBlockFile, ReadByte, mf_t.Busy_Bytes, mf_t.Number_Allocated_Blocks, 0, out UInt32 Block, out string Str);
                    return true;
                }
                ReadByte += Convert.ToUInt32(Size);
            }
            return false;
        }                                                     /*mvdir*/
        public bool RemoveDirectory(string NameDirectory, string CurrentPath, UInt32 IDUser, Users _us)
        {
            string ErrorMessage;
            char[] Value;
            string tmp = "";
            if (NameDirectory.Length > 25)
                Value = NameDirectory.Substring(0, 25).ToCharArray();
            else if (NameDirectory.Length == 25)
                Value = NameDirectory.ToCharArray();
            else
            {
                Value = new char[25];
                for (int i = 0; i < 25 - NameDirectory.Length; i++)
                    tmp += ' ';
                NameDirectory.ToCharArray().CopyTo(Value, 0);
                tmp.ToCharArray().CopyTo(Value, NameDirectory.Length);
            }
            char[] Temp1 = new char[25];
            tmp = "";
            for (int i = 0; i < 25 - _users.see_user.Login.Length; i++)
                tmp += ' ';
            _users.see_user.Login.CopyTo(Temp1, 0);
            tmp.ToCharArray().CopyTo(Temp1, _users.see_user.Login.Length);
            char[] Temp2 = new char[22];
            CurrentPath.CopyTo(0, Temp2, 0, CurrentPath.Length);
            NameDirectory.CopyTo(0, Temp2, CurrentPath.Length, NameDirectory.Length);
            for (int i = CurrentPath.Length + NameDirectory.Length; i < Temp2.Length; i++)
                Temp2[i] = ' ';
            var us = _us.GetUsers();
            foreach (var cur in us)
            {
                if (cur.Home_Directory.SequenceEqual(CurrentPath + NameDirectory))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Не возможно удалить директорию!");
                    Console.ResetColor();
                    return false;
                }
            }
            var _INFO = GetAllFilesInDirectory(CurrentPath, out UInt32 Last);
            bool Find = false;
            Struct.MFT mfT = new Struct.MFT();
            foreach (var TempVal in _INFO)
            {
                if (TempVal.File_Type == 'd' & TempVal.File_Name.SequenceEqual(Value) & TempVal.UserID != 0 & !(TempVal.File_Name.SequenceEqual(Temp1) & CurrentPath.SequenceEqual("/USERS/")))
                {
                    mfT = TempVal;
                    Find = true;
                    break;
                }
            }
            if (!Find)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Директории не существует!");
                Console.ResetColor();
                return false;
            }
            var _InfoMFT = SearchSystemInformationInMFT("MFT");
            UInt32 ReadBytes = 0;
            byte[] ReadArray;
            while (ReadBytes < _InfoMFT.Busy_Bytes)
            {
                var _mFT = new Struct.MFT();
                ReadArray = new byte[Size];
                ReadArray = ASFS.ReadBytes(_mFT.AdressBlockFile, ReadBytes, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ReadArray, out _mFT);
                if (_mFT.Base_Record_Number == mfT.Base_Record_Number && _mFT.File_Type == 'd' && _mFT.File_Name.SequenceEqual(mfT.File_Name))
                {
                    break;
                }
                ReadBytes += Convert.ToUInt32(Size);
            }
            UInt32 Block;
            do
            {
                ReadBytes += Convert.ToUInt32(Size);
                ReadArray = new byte[Size];
                ReadArray = ASFS.ReadBytes(_InfoMFT.AdressBlockFile, ReadBytes, Convert.ToUInt32(Size), null);
                ASFS.WriteFileByBytes(ReadArray, 0, ReadBytes - Convert.ToUInt32(Size), _InfoMFT.Busy_Bytes - Convert.ToUInt64(ReadArray.LongLength), _InfoMFT.Number_Allocated_Blocks, 0, out Block, out ErrorMessage);
            } while (ReadBytes < _InfoMFT.Busy_Bytes);
            _InfoMFT.Busy_Bytes -= Convert.ToUInt32(ReadArray.Length);
            byte[] b1 = FunctionsForFS.GetBytes(_InfoMFT);
            ASFS.WriteFileByBytes(b1, _InfoMFT.AdressBlockFile, 0, Convert.ToUInt64(ReadArray.Length), 1, 0, out Block, out ErrorMessage);
            ASFS.WriteFileByBytes(b1, CopyMFTBlock, 0, Convert.ToUInt32(ReadArray.Length), 1, 0, out Block, out ErrorMessage);
            return true;
        }               /*rmdir*/
        public bool ViewFileContents(string FileName)
        {
            string[] PathToFile = FileName.Split('/');
            string temp = "";
            for (int i = 0; i < PathToFile.Length - 1; i++)     /*название файла нам не нужно*/
                temp += PathToFile[i] + '/';
            if (!CheckPathOnExist(temp))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Файл не найден!");
                Console.ResetColor();
                return false;
            }
            if (!ExistFileInDirectory(temp, PathToFile[PathToFile.Length - 1], out Struct.MFT fT))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("В директории файл не найден!");
                Console.ResetColor();
                return false;
            }
            byte[] ReadArray = ASFS.ReadBytes(fT.AdressBlockFile, 0, Convert.ToUInt32(fT.Busy_Bytes), null);
            string ContentFile = Encoding.Unicode.GetString(ReadArray);
            Console.WriteLine(ContentFile);
            return true;
        }                                                                 /*cat*/
        public bool EditFile(string FileName)
        {
            string[] PathToFile = FileName.Split('/');
            string tmp = "";
            for (int i = 0; i < PathToFile.Length - 1; i++)     /*название файла нам не нужно*/
                tmp += PathToFile[i] + '/';
            if (!CheckPathOnExist(tmp))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Файл не найден!");
                Console.ResetColor();
                return false;
            }
            if (!ExistFileInDirectory(tmp, PathToFile[PathToFile.Length - 1], out Struct.MFT fT))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("В директории файл не найден!");
                Console.ResetColor();
                return false;
            }
            byte[] ReadArray = ASFS.ReadBytes(fT.AdressBlockFile, 0, Convert.ToUInt32(fT.Busy_Bytes), null);
            char[] ContentFile = Encoding.Unicode.GetChars(ReadArray);      /*Здесь работаем с char*/
            Console.Write(new string(ContentFile));
            List<char> EditContent = new List<char>();
            for (int i = 0; i < ContentFile.Length; i++)                    /*Копируем контент файла*/
                EditContent.Add(ContentFile[i]);
            ConsoleKeyInfo ReadInput;
            do
            {
                ReadInput = Console.ReadKey();
                if ((ConsoleModifiers.Control & ReadInput.Modifiers) != 0)
                    if (ReadInput.Key == ConsoleKey.D)
                    {
                        break;
                    }                                           
                if (ReadInput.Key == ConsoleKey.Backspace)
                {
                    if (EditContent.Count != 0)
                        EditContent.Remove(EditContent[EditContent.Count - 1]);
                }
                else
                    EditContent.Add(ReadInput.KeyChar);
            } while (true);
            PathToFile = FileName.Split('/');
            string PTF = "/";
            for (int i = 0; i < PathToFile.Length - 1; i++)
            {
                if (PathToFile[i] != "")
                    PTF += PathToFile[i] + '/';
            }
            string NF = PathToFile[PathToFile.Length - 1];

            char[] Value;
            if (NF.Length > 25)
                Value = NF.Substring(0, 25).ToCharArray();
            else if (NF.Length == 25)
                Value = NF.ToCharArray();
            else
            {
                Value = new char[25];
                string str = "";
                for (int i = 0; i < 25 - NF.Length; i++)
                    str += ' ';
                NF.ToCharArray().CopyTo(Value, 0);
                str.ToCharArray().CopyTo(Value, NF.Length);
            }
            var _INFO = GetAllFilesInDirectory(PTF, out UInt32 LAST);
            bool Find = false;
            Struct.MFT s_mft = new Struct.MFT();
            foreach (var search in _INFO)
            {
                if (search.File_Type == '-' && search.File_Name.SequenceEqual(Value))
                {
                    Find = true;
                    s_mft = search;
                    break;
                }
            }
            if (!Find)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Файла не существует!");
                Console.ResetColor();
                return false;
            }
            var SysMFT = SearchSystemInformationInMFT("MFT");
            UInt32 ReadBytes = 0;
            Struct.MFT editMFT = new Struct.MFT();
            while (ReadBytes < SysMFT.Busy_Bytes)
            {
                editMFT = new Struct.MFT();
                ReadArray = new byte[Size];
                ReadArray = ASFS.ReadBytes(SysMFT.AdressBlockFile, ReadBytes, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ReadArray, out editMFT);
                if (editMFT.Base_Record_Number == s_mft.Base_Record_Number && editMFT.File_Type == '-' && editMFT.File_Name.SequenceEqual(s_mft.File_Name))
                    break;
                ReadBytes += Convert.ToUInt32(Size);
            }
            if (editMFT.AdressBlockFile == 0)
                return false;
            ASFS.DeleteFile(editMFT.AdressBlockFile);
            editMFT.Busy_Bytes = Convert.ToUInt32(Encoding.Unicode.GetBytes(EditContent.ToArray()).Length);
            editMFT.Number_Allocated_Blocks = Convert.ToUInt32(Math.Ceiling(Convert.ToDouble(Encoding.Unicode.GetBytes(EditContent.ToArray()).Length) / ASFS.BootBlockSize));
            ASFS.WriteFileByBytes(Encoding.Unicode.GetBytes(EditContent.ToArray()), 0, 0, 0, 0, CopyMFTBlock, out UInt32 Block, out string ErrorMessage);
            editMFT.AdressBlockFile = Convert.ToUInt32(ErrorMessage);
            ReadArray = FunctionsForFS.GetBytes(editMFT);
            ASFS.WriteFileByBytes(ReadArray, SysMFT.AdressBlockFile, ReadBytes, SysMFT.Busy_Bytes, SysMFT.Number_Allocated_Blocks, 0, out Block, out ErrorMessage);
            Console.WriteLine();
            return true;
        }                                                                         /*cat <*/
        public bool MoveFile(string FromWhere, string Where)
        {
            string[] Source = FromWhere.Split('/');
            string[] Destination = Where.Split('/');
            string SourcePath = "", DestinationPath = "";
            for (int i = 0; i < Source.Length - 1; i++)
                SourcePath += Source[i] + '/';
            for (int i = 0; i < Destination.Length - 1; i++)
                DestinationPath += Destination[i] + '/';
            if (!CheckPathOnExist(SourcePath))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Исходный путь не найден!");
                Console.ResetColor();
                return false;
            }
            if (!ExistFileInDirectory(SourcePath, Source[Source.Length - 1], out Struct.MFT SourceMFT))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Файл не существует в директории!");
                Console.ResetColor();
                return false;
            }
            if (!CheckPathOnExist(DestinationPath))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Путь не найден!");
                Console.ResetColor();
                return false;
            }
            Struct.MFT DestinationMFT;
            if (FileInDirectory(DestinationPath,Destination[Destination.Length-1],out DestinationMFT.File_Name))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Такой файл уже есть в директори!");
                Console.ResetColor();
                return false;
            }
            SourceMFT.Base_Record_Number = GetIDForCurrentPath(DestinationPath);
            var NMFT = SearchSystemInformationInMFT("MFT");
            UInt32 ReadBytes = 0;
            byte[] ReadArray;
            while (ReadBytes < NMFT.Busy_Bytes)
            {
                var m = new Struct.MFT();
                ReadArray = new byte[Size];
                ReadArray = ASFS.ReadBytes(NMFT.AdressBlockFile, ReadBytes, Convert.ToUInt32(Size), null);
                FunctionsForFS.FromBytes(ReadArray, out m);
                if (m.Record_Number == SourceMFT.Record_Number && m.File_Type == '-' && m.File_Name.SequenceEqual(SourceMFT.File_Name))
                    break;
                ReadBytes += Convert.ToUInt32(Size);
            }
            DestinationMFT.File_Name.CopyTo(SourceMFT.File_Name, 0);
            ReadArray = FunctionsForFS.GetBytes(SourceMFT);
            ASFS.WriteFileByBytes(ReadArray, SourceMFT.AdressBlockFile, ReadBytes, SourceMFT.Busy_Bytes, SourceMFT.Number_Allocated_Blocks, 0, out UInt32 Block, out string ErrorMessage);  /*Дописать!*/
            return true;
        }                                                          /*mv*/
        public bool ChangeMode(string Path, string AccessLevel, UInt32 UserId)
        {
            string[] PathToFile = Path.Split('/');
            string NameFile = PathToFile[PathToFile.Length - 1];
            char[] Value;
            if (NameFile.Length > 25)
                Value = NameFile.Substring(0, 25).ToCharArray();
            else if (NameFile.Length == 25)
                Value = NameFile.ToCharArray();
            else
            {
                Value = new char[25];
                string str = "";
                for (int i = 0; i < 25 - NameFile.Length; i++)
                    str += ' ';
                NameFile.ToCharArray().CopyTo(Value, 0);
                str.ToCharArray().CopyTo(Value, NameFile.Length);
            }
            byte[] ReadArray;
            var _INFO = GetAllFilesInDirectory(Path, out UInt32 Last);
            foreach (var Inf in _INFO)
            {
                if (Inf.File_Type == '-' & Inf.File_Name.SequenceEqual(Value) & Inf.UserID == UserId)
                {
                    var SysMFT = SearchSystemInformationInMFT("MFT");
                    UInt32 ReadBytes = 0;
                    Struct.MFT editMFT = new Struct.MFT();
                    while (ReadBytes < SysMFT.Busy_Bytes)
                    {
                        editMFT = new Struct.MFT();
                        ReadArray = new byte[Size];
                        ReadArray = ASFS.ReadBytes(SysMFT.AdressBlockFile, ReadBytes, Convert.ToUInt32(Size), null);
                        FunctionsForFS.FromBytes(ReadArray, out editMFT);
                        if (editMFT.File_Name.SequenceEqual(Value) & editMFT.File_Type == '-' & editMFT.UserID == UserId)
                            break;
                        ReadBytes += Convert.ToUInt32(Size);
                    }
                    editMFT.Access_Level = AccessLevel.ToCharArray();
                    ReadArray = FunctionsForFS.GetBytes(editMFT);
                    ASFS.WriteFileByBytes(ReadArray, SysMFT.AdressBlockFile, ReadBytes, SysMFT.Busy_Bytes, SysMFT.Number_Allocated_Blocks, 0, out UInt32 Block, out string ErrorMessage);
                }
            }
            return true;
        }                                        /*chmod*/
        #endregion
    }
}
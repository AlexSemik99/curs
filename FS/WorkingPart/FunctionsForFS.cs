﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Runtime.InteropServices;


namespace FS.WorkingPart
{
    class FunctionsForFS
    {
        public static byte[] GetBytes<T>(T str)
        {
            int Size = Marshal.SizeOf(typeof(T));
            byte[] arr = new byte[Size];
            IntPtr ptr = Marshal.AllocHGlobal(Size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, Size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }
        public static void FromBytes<T>(byte[] array, out T structure)
        {
            int Size = Marshal.SizeOf(typeof(T));
            IntPtr ptr = Marshal.AllocHGlobal(Size);
            Marshal.Copy(array, 0, ptr, Size);
            structure = (T)Marshal.PtrToStructure(ptr, typeof(T));
            Marshal.FreeHGlobal(ptr);
        }
        public static string ConvertAccessLevel(string Input)
        {
            string level = "";
            string[] AL = { "---", "--x", "-w-", "-wx", "r--", "r-x", "rw-", "rwx" };
            for(int i = 0; i < Input.Length; i++)
            {
                for(int j = 0; j < AL.Length; j++)
                {
                    if (Convert.ToInt32(Convert.ToString(Input[i])) == j)
                        level += AL[j];
                }
            }
            return level;
        }
    }
    static class Hashing
    {
        public static char[] Hash(string InputValue, int Length)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(InputValue));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("X2"));
                }
                return sb.ToString().Substring(0, Length).ToCharArray();
            }
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace FS
{
    class ASFS
    {
        WorkingPart.BootFS Boot = new WorkingPart.BootFS();
        WorkingPart.MFT MFT = new WorkingPart.MFT();
        WorkingPart.Users Users = new WorkingPart.Users();
        WorkingPart.ASFS _ASFS = new WorkingPart.ASFS();
        WorkingPart.Groups Groups = new WorkingPart.Groups();
        private void WriteBlocks(byte[] Data, FileStream fs, UInt32 Blocks, UInt32 CountBusyBlock, int BootSectorSize)
        {
            fs.Position = BootSectorSize + Blocks * CountBusyBlock;
            fs.Write(Data, 0, Data.Length);
        }
        public void Initialize(string UserName, string UserLogin, string UserPassword, UInt32 SizeDisk)
        {
            WorkingPart.BootFS boot = new WorkingPart.BootFS();
            WorkingPart.Users users = new WorkingPart.Users();
            WorkingPart.Groups groups = new WorkingPart.Groups();
            WorkingPart.MFT mft = new WorkingPart.MFT();
            WorkingPart.ASFS asfs = new WorkingPart.ASFS();
            UInt32 ByteSizeDisk = Convert.ToUInt32(SizeDisk * 1024 * 1024) - Convert.ToUInt32(boot.SizeStruct * 2);
            boot.Initialize(asfs.SelectionSizeBlock(ByteSizeDisk), 0, asfs.CountBlock / 2);
            groups.AddGroup("Admin", 1);
            groups.AddGroup("User", 2);
            users.AddUser(UserName, UserLogin, UserPassword, 1);

            UInt32 Size5MFTRecords = Convert.ToUInt32(mft.GetSizeRecord(7, boot.GetBlockSize()));
            mft.AddMFTRecord(Size5MFTRecords, "MFT", Convert.ToUInt32(mft.GetSizeRecord(7, 1)), 0, 0, "rwx", DateTime.Now, "rwxrwxrwx", "-", 0);
            asfs.AddRecordInFAT(0, Size5MFTRecords);
            asfs.AddRecordInFAT(asfs.CountBlock / 2, 1);

            UInt32 FS = asfs.GetBlockSize(boot.GetBlockSize());
            mft.AddMFTRecord(FS, "FS", Convert.ToUInt32(asfs.GetSizeBytes()), 0, 0, "rwx", DateTime.Now, "rwxrwxrwx", "-", Size5MFTRecords);
            asfs.AddRecordInFAT(Size5MFTRecords, FS);

            UInt32 GR = groups.GetBlockSize(boot.GetBlockSize());
            mft.AddMFTRecord(GR, "GROUPS", Convert.ToUInt32(groups.GetSizeBytes()), 0, 0, "rwx", DateTime.Now, "rwxrwxrwx", "-", Size5MFTRecords + FS);
            asfs.AddRecordInFAT(Size5MFTRecords + FS, GR);

            UInt32 US = users.GetBlockSize(boot.GetBlockSize());
            mft.AddMFTRecord(US, "USERS", Convert.ToUInt32(users.GetSizeBytes()), 0, 0, "rwx", DateTime.Now, "rwxrwxrwx", "-", Size5MFTRecords + FS + GR);
            asfs.AddRecordInFAT(Size5MFTRecords + FS + GR, US);

            mft.AddMFTRecord(0, ".", 0, 0, 0, "rwx", DateTime.Now, "rwxrwxrwx", "-", 0);

            mft.AddMFTRecord(0, "USERS", 0, 5, 0, "---", DateTime.Now, "rwx---rwx", "d", 0);
            mft.AddMFTRecord(0, UserLogin, 0, 6, 1, "---", DateTime.Now, "rwx---rwx", "d", 0);
            using (FileStream stream = new FileStream(WorkingPart.ASFS.Path_To_File, FileMode.Create))
            {
                stream.SetLength(Convert.ToInt32(SizeDisk * 1024 * 1024));
                stream.Write(boot.ByteConvert(), 0, boot.SizeStruct);

                WriteBlocks(mft.Convert_To_byte(), stream, 0, boot.GetBlockSize(), boot.SizeStruct);
                WriteBlocks(mft.Convert_To_byte(), stream, asfs.CountBlock / 2, boot.GetBlockSize(), boot.SizeStruct);

                WriteBlocks(asfs.Convert_To_byte(), stream, Size5MFTRecords, boot.GetBlockSize(), boot.SizeStruct);
                WriteBlocks(groups.Convert_To_byte(), stream, Size5MFTRecords + FS, boot.GetBlockSize(), boot.SizeStruct);
                WriteBlocks(users.Convert_To_byte(), stream, Size5MFTRecords + FS + GR, boot.GetBlockSize(), boot.SizeStruct);

                stream.Position = Convert.ToInt32(SizeDisk * 1024 * 1024) - boot.SizeStruct;
                stream.Write(boot.ByteConvert(), 0, boot.SizeStruct);
                stream.Close();
            }
        }
        public bool StartSystem()
        {
            Console.Title = "ASFS";
            Struct.MFT asfs, group, user;
            if (!Boot.ReadBootSector())
                return false;
            _ASFS.Initialize(Boot.GetBlockSize(), Boot.SizeStruct);
            MFT.Initialize(Boot.sb.Num_Copy_MFT, Boot.sb.Num_Block, _ASFS);
            asfs = MFT.GetFS();
            if (asfs.File_Name == null)
                return false;
            _ASFS.Initialize2(asfs.AdressBlockFile, asfs.Busy_Bytes);
            group = MFT.SearchSystemInformationInMFT("GROUPS");
            if (group.File_Name == null)
                return false;
            user = MFT.SearchSystemInformationInMFT("USERS");
            if (user.File_Name == null)
                return false;
            Users.Initialize(_ASFS, user);
            Groups.Initialize(_ASFS, group);
            Console.Clear();
            if (!Login())
                return false;
            string name = new string(Users.see_user.User_Name).Trim(' ');
            Console.WriteLine($"Добро пожаловать \"{name}\"!");
            string readme = "";
            using (StreamReader sr = new StreamReader(@"ReadMe.cn"))
            {
                readme = sr.ReadToEnd();
            }
            char[] README = readme.ToCharArray();
            byte[] b = Encoding.Unicode.GetBytes(README);
            MFT.MakeFile(Convert.ToString(Users.see_user.Home_Directory), b, "---", "rwxrwxrwx", Users.see_user.Id_User);
            ConsoleCommand(new string(Users.see_user.Home_Directory));
            return true;
        }
        public bool Login()
        {
            ConsoleKeyInfo key;
            while (true)
            {
                Console.Clear();
                Console.Write("Логин: ");
                Console.ForegroundColor = ConsoleColor.Green;
                string InputLogin = Console.ReadLine();
                Console.ResetColor();
                Console.Write("Пароль: ");
                Console.ForegroundColor = ConsoleColor.Green;
                string InputPassword = Console.ReadLine();
                Console.ResetColor();
                if (Users.LoginInSystem(InputLogin, InputPassword))
                    return true;
                Console.Clear();
                Console.WriteLine("Такого пользователя не существует. \nНажмите любой символ для повторной попытки. \n(Чтобы выйти нажмите [ESC])");
                Console.ResetColor();
                if ((key = Console.ReadKey()).Key == ConsoleKey.Escape)
                    return false;
            }
        }
        public void ConsoleCommand(string Directory)
        {
            Console.Clear();
            while (true)
            {
                MFT._groups = Groups;
                MFT._users = Users;
                Console.Title = new string(Users.see_user.User_Name).Trim(' ');
                Console.Write("#" + Directory.Trim(' ') + ">>");
                Console.ForegroundColor = ConsoleColor.Green;
                string ConsoleCommand = Console.ReadLine();
                Console.ResetColor();
                string[] Command = ConsoleCommand.Split(' ');
                switch (Command[0])
                {
                    case "ls":
                        MFT.PrintFiles(Directory.Trim(' ') + "/", Users.see_user.Id_User);
                        break;
                    case "cd":
                        if (Command[1][0] == '/' && Command[1][Command[1].Length - 1] == '/')
                        {
                            if (MFT.CheckPathOnExist(Command[1]))
                                Directory = Command[1];
                        }
                        else if (Command[1][Command[1].Length - 1] != '/')
                        {
                            if (MFT.CheckPathOnExist(Directory.Trim(' ') + Command[1] + "/"))
                                Directory = Directory.Trim(' ') + Command[1] + "/";
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Каталога не существует!");
                                Console.ResetColor();
                            }
                        }
                        else if (Command.Length == 2 && (Command[1][0] == '.' && Command[1][1] == '.' && Command[1][2] == '/'))
                        {
                            try
                            {
                                int i = 0;
                                for (i = Directory.Length - 1; i > 0; i--)
                                    if (Directory[i - 1] == '/')
                                        break;
                                char[] Dir = new char[i];
                                Directory.CopyTo(0, Dir, 0, i);
                                Directory = new string(Dir).Trim(' ');
                                if (!MFT.CheckPathOnExist(Directory))
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Каталога не существует.");
                                    Console.ResetColor();
                                }
                            }
                            catch (Exception)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Ошибка при изменении текущего рабочего каталога!");
                                Console.ResetColor();
                            }
                        }
                        else
                        {
                            if (MFT.CheckPathOnExist(Directory + Command[1]))
                                Directory = Directory + Command[1];
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Каталога не существует.");
                                Console.ResetColor();
                            }
                        }
                        break;
                    case "clear":
                        Console.Clear();
                        break;
                    case "exit":
                        Environment.Exit(0);
                        break;
                    case "cat":
                        List<char> Info = new List<char>();
                        ConsoleKeyInfo ReadInput;
                        char[] NeedArrayToUnicode;
                        byte[] ArrayForCreateFile;
                        if (Command.Length == 3)
                        {
                            if (Command[1] == ">" & Command[2] != null)
                            {
                                do
                                {
                                    ReadInput = Console.ReadKey();
                                    if ((ConsoleModifiers.Control & ReadInput.Modifiers) != 0)
                                        if (ReadInput.Key == ConsoleKey.D)
                                            break;                               /*Выход при нажатии Ctrl + D*/
                                    if (ReadInput.Key == ConsoleKey.Backspace)
                                    {
                                        if (Info.Count != 0)
                                        {
                                            Info.Remove(Info[Info.Count - 1]);
                                        }
                                    }
                                    else
                                    {
                                        Info.Add(ReadInput.KeyChar);
                                    }
                                } while (true);
                                NeedArrayToUnicode = Info.ToArray();
                                ArrayForCreateFile = Encoding.Unicode.GetBytes(NeedArrayToUnicode);
                                if (Command[1][0] == '/')
                                    MFT.MakeFile(Command[2], ArrayForCreateFile, "---", "rwxrwxrwx", Users.see_user.Id_User);
                                else
                                    MFT.MakeFile(Directory.Trim(' ') + "/" + Command[2], ArrayForCreateFile, "---", "rwxrwxrwx", Users.see_user.Id_User);
                                Console.WriteLine();
                            }
                            else if (Command[1] == "<" & Command[2] != null)
                            {
                                MFT.EditFile(Directory.Trim(' ') + "/" + Command[2]);
                            }
                        }
                        if (Command.Length == 2)
                        {
                            MFT.ViewFileContents(Directory.Trim(' ') + "/" + Command[1]);
                        }
                        break;
                    case "chmod":
                        if (Command.Length == 3)
                        {
                            if (Command[1] != null & Command[1].Length == 3 & Command[2] != null)
                            {
                                string Level = WorkingPart.FunctionsForFS.ConvertAccessLevel(Command[1]);
                                if (!MFT.CheckPathOnExist(Command[2] + Directory.Trim(' ')))
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Файла не существует!");
                                    Console.ResetColor();
                                }
                                else
                                {
                                    MFT.ChangeMode(Directory.Trim(' ') + "/" + Command[2], Level, Users.see_user.Id_User);
                                }
                            }
                        }
                        break;
                    case "mkdir":
                        if (Command[1] != null)
                        {
                            MFT.MakeDirectory(Command[1], Directory.Trim(' ') + "/", Users.see_user.Id_User);
                        }
                        break;
                    case "rm":
                        if (Command.Length == 2)
                        {
                            MFT.RemoveFile(Command[1], Directory.Trim(' ') + "/", Users.see_user.Id_User);
                        }
                        break;
                    case "rmdir":
                        if (Command.Length == 2)
                        {
                            MFT.RemoveDirectory(Command[1], Directory.Trim(' '), Users.see_user.Id_User, Users);
                        }
                        break;
                    case "mv":
                        if (Command.Length == 3)
                        {
                            if (Command[1] == null && Command[2] == null)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Не все параметры заданы!");
                                Console.ResetColor();
                            }
                            MFT.MoveFile(Directory.Trim(' ') + "/" + Command[1], Directory.Trim(' ') + "/" + Command[2] + "/" + Command[1]);
                        }
                        break;
                    case "mvdir":
                        if (Command.Length == 3)
                        {
                            if (Command[1] == "" || Command[2] == "")
                            {
                                Console.WriteLine("Ошибка: Введите исходную и удаленную папку");
                            }
                            else if (Command[1][0] == '/' && Command[1][0] == '/')
                            {
                                MFT.MoveDirectory(Command[1], Command[2], Users.see_user.Id_User, false);
                            }
                            else if (Command[1][0] == '/' && Command[2][0] != '/')
                            {
                                MFT.MoveDirectory(Command[1], Directory.Trim(' ') + Command[2] + "/", Users.see_user.Id_User, false);
                            }
                            if (Command[1][0] != '/' && Command[2][0] == '/')
                            {
                                MFT.MoveDirectory(Directory.Trim(' ') + Command[1] + "/", Command[2], Users.see_user.Id_User, false);
                            }
                            else
                                MFT.MoveDirectory(Directory.Trim(' ') + "/" + Command[1] + "/", Directory.Trim(' ') + "/" + Command[2] + "/", Users.see_user.Id_User, false);
                        }
                        break;
                    case "out":
                        Console.Clear();
                        Login();
                        Console.Title = "ASFS";
                        Console.Clear();
                        Directory = new string(Users.see_user.Home_Directory).Trim(' ');
                        break;
                    case "info":
                        if (Command.Length == 1)
                        {
                            Console.WriteLine($"{"Файловая система:"}{"ASFS",13}");
                            Console.WriteLine($"{"Разработчик:",17}{"ст.гр.ПИ 16 - а Семик А.О",34}");
                            Console.WriteLine($"{"Прототип ФС:",17}{"FAT",12}");
                            Console.WriteLine($"{"Город:",17}{"Донецк",15}");
                        }
                        else if (Command.Length == 2)
                        {
                            if (Command[1] == "user")
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine($"{"ID"}{"ИМЯ ПОЛЬЗОВАТЕЛЯ",20}{"ГРУППА",16}{"ДИРЕКТОРИЯ",20}");
                                Console.ResetColor();
                                if (Users.see_user.Id_Group == 1)
                                    Console.WriteLine($"{Users.see_user.Id_User}{new string(Users.see_user.User_Name).Trim(' '),17}{"Admin",19}{new string(Users.see_user.Home_Directory).Trim(' '),21}");
                                else
                                    Console.WriteLine($"{Users.see_user.Id_User}{new string(Users.see_user.User_Name).Trim(' '),17}{"Users",19}{new string(Users.see_user.Home_Directory).Trim(' '),21}");
                            }
                            if (Command[1] == "users")
                            {

                            }
                        }
                        break;
                    case "adduser":
                        if (Users.see_user.Id_User == 1)
                            Users.CreateUser(Groups, MFT, Boot.GetCopyMFT());
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("У вас нет прав администратора!");
                            Console.ResetColor();
                        }
                        break;
                    case "rmuser":
                        //Console.Write("Логин: ");
                        //string UserLogin = Console.ReadLine();
                        //Console.Write("Пароль: ");
                        //string UserPassword = Console.ReadLine();
                        //if (Users.RemoveUser(UserLogin, UserPassword, Boot.GetCopyMFT(), MFT))
                        //    MFT.RemoveDirectory(UserLogin, $"/USERS/{UserLogin}/", Users.see_user.Id_User, Users);
                        //else
                        //{
                        //    Console.ForegroundColor = ConsoleColor.Red;
                        //    Console.WriteLine("У вас нет прав администратора!");
                        //    Console.ResetColor();
                        //}
                        break;
                    case "help":
                        Console.WriteLine($"{"ls"} {"Утилита вывода информации о файлах или каталогах ",76}");
                        Console.WriteLine($"{"out"} {"Выход из учётной записи",49}");
                        Console.WriteLine($"{"exit"}{"Закрытие программы",44}");
                        Console.WriteLine($"{"info"}{"Информация о системе",46}");
                        Console.WriteLine($"{"rmuser"}{"Команда для удаления пользователя",57}");
                        Console.WriteLine($"{"adduser"}{"Команда для создания нового пользователя",63}");
                        Console.WriteLine($"{"cd + path"} {"Команда для изменения текущего рабочего каталога",68}");
                        Console.WriteLine($"{"rm + name_file"} {"Утилита вывода информации о файлах или каталогах ",64}");
                        Console.WriteLine($"{"cat + name_file",15} {"Утилита, выводящая содержание некоторых файлов",60}");
                        Console.WriteLine($"{"cat + \">\" name_file",10} {"Утилита, записывающая данные в файл",45}");
                        Console.WriteLine($"{"cat + \"<\" name_file",10} {"Утилита, изменияющая данные в файле",45}");
                        Console.WriteLine($"{"mkdir + name_directory"}{"Команда для создания новых каталогов",44}");
                        Console.WriteLine($"{"rmdir + name_directory"}{"Команда для удаления каталогов",38}");
                        Console.WriteLine($"{"mv + source + destination"}{"Утилита, используемая для перемещения файлов.",50}");
                        break;
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace FS.Struct
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    struct User
    {
        public UInt32 Id_User;                              //ID пользователя
        public UInt32 Id_Group;                             //ID группы
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 25)]
        public char[] User_Name;                            //= new char[25] - Имя пользователя
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 27)]
        public char[] Home_Directory;                       // = new char[32] - Домашняя директория
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public char[] Login;                                // = new char[20] - Логин
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
        public char[] Hesh_Password;                        //= new char[15] - Хеш пароль пользователя
    }
}
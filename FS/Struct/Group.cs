﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;


namespace FS.Struct
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    struct Group
    {
        public UInt32 Id_Group;                         //ID группы
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
        public char[] Name_Group;                      // = new char[20] - Название группы
        public UInt32 Access_Level;                    //= 0;
    }
}
﻿using System;
using System.Runtime.InteropServices;

namespace FS.Struct
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    struct MFT
    {
        public UInt32 Record_Number;                //Номер записи
        public UInt32 Number_Allocated_Blocks;      //Кол-во выделенных блоков
        public UInt32 Busy_Bytes;                   //Кол-во занятых байтов
        public UInt32 Base_Record_Number;           //Номер базовой записи
        public UInt32 UserID;                       //Какой пользователь создал файл
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public char[] File_Attributes;              //= new char[2] Атрибуты файла 
        public long Create_File;                    //Время последней модификации файла
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
        public char[] Access_Level;                 //= new char[9] Уровень доступа
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 25)]
        public char[] File_Name;                    //= new char[25] - Название файла
        public char File_Type;                      //Тип файла
        public UInt32 AdressBlockFile;              //=0
    }
}
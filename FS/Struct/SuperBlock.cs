﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Struct
{
    struct SuperBlock
    {
        public UInt32 Claster_Size;         //= 512 - Размер кластера
        public UInt32 Num_Block;            //= 1 - Номер блока файла MFT
        public UInt32 CRC;                  // = 0 - Контроль данных
        public UInt32 Num_Copy_MFT;         // Копия MFT
    }
}